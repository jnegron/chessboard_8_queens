
col = "abcdefghi"
fil = "12345678"
solutions_8queens = []
def getPosInt(letra):
    if letra in col:
        return col.find(letra)
    if letra in fil:
        return fil.find(letra)   
def string2list(input_string):
    array_pos = []
    n = len(input_string)
    for i in range(0, n/2):
        array_pos.append(input_string[2*i:2*i+2])
    return array_pos
def searchInFil(position):
    positions = []
    for i in range(0, 8):
        if (i != getPosInt(position[0])):
            positions.append(col[i]+position[1])
    return positions
def searchInCol(position):
    positions = []
    for i in range(0, 8):
        if (i != getPosInt(position[1])):
            positions.append(position[0]+fil[i])
    return positions
def eqPuntoPendiente(m, position, value):
    punto = (getPosInt(position[0]), getPosInt(position[1]))
    y = m*(value - punto[0]) + punto[1]
    if (y >= 0 and y <= 7):
        return col[value]+fil[y]
    return None
def searchPositions(position):
    positions = []
    n_col = getPosInt(position[0])
    n_fil = getPosInt(position[1])
    for i in range(0, 8):
        if (i != n_fil):
            positions.append(position[0]+fil[i])
        if (i != n_col):
            positions.append(col[i]+position[1])
            if (eqPuntoPendiente(1, position, i) != None):
                positions.append(eqPuntoPendiente(1, position, i))
            if (eqPuntoPendiente(-1, position, i) != None):
                positions.append(eqPuntoPendiente(-1, position, i))
    return positions
def is8QueensSolution(input_string):
    collisions = []
    if(len(input_string) == 16):
        list_pos = string2list(input_string)
        for i in range(0, 8):
            occupied_by_queen_i  = searchPositions(list_pos[i])
            for j in range(0, 8):
                if i != j :
                    if (list_pos[j] in occupied_by_queen_i) and ([list_pos[j], list_pos[i]] not in collisions):
                        collisions.append([list_pos[i], list_pos[j]])
        if len(collisions) > 0:
            string_collisions = ""
            for collision in  collisions:
                string_collisions += collision[0] + " - "  + collision[1] + ","
            return [False, "Is not a valid solution for the 8 queens problem because exists collisions between: " +string_collisions[:-1]]

        return [True, "Is a valid solution for the 8 queens problem"]
    else:
        return [False, "insuficientes parametros para ser valido"]
def getPositionsBoard():
    board = []
    for i in range(0, 8):
        for j in range(0, 8):
            board.append(col[i]+fil[j])
    return board
def setIntersection(a, b, element):
    c = []
    for e in a:
        if e not in b and e != element:
            c.append(e)
    return c
def cellsAvailables(cell_i_availables, cell_i):
    return setIntersection(cell_i_availables,searchPositions(cell_i), cell_i)

def getAllSolutions(cell_i_availables, tentative_solution):
    if len(cell_i_availables) == 0 :
        string_solution = "".join(map(str, tentative_solution))
        if is8QueensSolution(string_solution)[0] and set(tentative_solution) not in solutions_8queens:
            print string_solution
            solutions_8queens.append(set(tentative_solution))
            return tentative_solution
    else:
        for cell_i in cell_i_availables:
            getAllSolutions(cellsAvailables(cell_i_availables,cell_i), tentative_solution + [cell_i])


def main():
    """
    print is8QueensSolution("a5c8b1d4e2f7g3h6")[1]
    print is8QueensSolution("a5c8b1d4e2f7g3h7")[1]
    print is8QueensSolution("a8c8b1d4e2f7g3h6")[1]
    print is8QueensSolution("a8c8b1d4e2f7g3h7")[1]
    """
    for i in range(0, 8):
        for j in range(0,8):
            pos = col[i]+fil[j]
            getAllSolutions(cellsAvailables(getPositionsBoard(), pos), [pos])
    
    getAllSolutions(cellsAvailables(getPositionsBoard(), "a1"), ["a1"])
    print len(solutions_8queens)
    print solutions_8queens
    #first_available_list = getPositionsBoard()
    #print set(first_available_list).difference(["a1"] + searchPositions("a1"))
if __name__ == '__main__':
	main()
"""
tentative_solution = []
tentative_solutions = []
#print cells_availables
tentative_solution.append(cells_availables[0])
occupped_position_i = searchPositions(cells_availables[0])
cells_availables = list(set(cells_availables).intersection(occupped_position_i))
#print cells_availables
for cell in cells_availables:
    tentative_solutions.append([tentative_solution[0], cell])
print tentative_solutions
return ""
"""